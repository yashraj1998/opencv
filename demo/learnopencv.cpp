// #include <Windows.h>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
// #include <opencv2\highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>
#include <opencv2/video/tracking.hpp>
#include<iostream>

using namespace cv;
using namespace std;

int main()
{

    auto width = 200;
    auto height = 200;
    auto outW = 200;
    auto outH = 200;
    auto smallImgData = new uint8_t[width*height];//creating buffer
    std::cout << "pointer<>" << (void*)smallImgData << std::endl; // ptr1

    uint8_t value = 5;
    for (auto i = 0; i < 1; i++)
    {
        auto img = imread("frame.jpg", -1);///output will be stored in a raw image
        cv::Mat smallImg(outH, outW, CV_8UC1, smallImgData);
        std::cout << "intermediate<>" << (void*)smallImg.data  << "<>" << (void*)(&value) << std::endl; // ptr2
        smallImg.data = smallImgData;//.data is a buffer which will point to buffer which was created by us in line ni 21 smallImgdata
        cv::resize(img, smallImg, Size(outW, outH));
        cout <<  "smallImg<>" << (void*)smallImg.data << endl; // ptr3
        cout<<smallImg.data[0];
        uchar* buffer = smallImg.data; 
        ostringstream filename;
        filename << "Frame_1.raw";
       // std::ofstream outfile (filename.str().c_str(), ios::out | ios::binary);
        filename.write ((char*)(buffer), smallImg.total());  // In byte so frame.total() should be enough ?
       // outfile.close();
    }

    delete[] smallImgData;

    return 0;
}
